# README

Enertiv API Sample Client for accessing data via Session or OAuth based authentication using Python.

# Quick Start

```bash
$ python api_client/get_client.py USER_NAME PASSWORD CLIENT_ID CLIENT_SECRET
# Returns a list of clients similar to https://api.enertiv.com/api/client/
```

# Using as a Dependency

## Installation

This package can be installed using pip.

```bash
$ pip install -e git+https://bitbucket.org/enertiv/api_client.git#egg=api_client
```

## Using in a Project

The api_client only needs to be instantiated once to establish the session and retrieve a access token, which can then be reused. Access token management is handled automatically within the client.

Identifiers (UUIDs) for location/sublocation/equipment can be determined by reviewing the documentation at https://api.enertiv.com/docs

```python
from api_client.enertiv_oauth_client import EnertivOAuthClient

client = EnertivOAuthClient(SERVER_USER, SERVER_PASSWORD, "https", "api.enertiv.com", 443, CLIENT_ID, CLIENT_SECRET)

response1 = client.get("https://api.enertiv.com/api/client")
print response1.json()

response2 = client.get("https://api.enertiv.com/api/location/553232d8-0f87-4cab-86de-263934829f7a/characteristic/")
print response2.json()

response3 = client.get("https://api.enertiv.com/api/location/553232d8-0f87-4cab-86de-263934829f7a/temperature/?fromTime=2014-01-01T05%3A00%3A00.000Z&toTime=2014-02-01T05%3A00%3A00.000Z&interval=hour")
print response3.json()
```

## curl equivalent

The python client achieves the equivalent as the following curl, in a convenient wrapper.

You can use the command line to test that your local configuration is working:
```
curl -X POST -d "client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET&grant_type=password&username=YOUR_USERNAME&password=YOUR_PASSWORD" https://api.enertiv.com/oauth2/access_token/
```
You should get a response that looks something like this:
```
{"access_token": "<your-access-token>", "scope": "read", "expires_in": 86399, "refresh_token": "<your-refresh-token>"}
```
Access the API

The only thing needed to make the OAuth2Authentication class work is to insert the access_token you've received in the Authorization request header.

The command line to test the authentication looks like:
```
curl -H "Authorization: Bearer <your-access-token>" https://api.enertiv.com/api/client/
```